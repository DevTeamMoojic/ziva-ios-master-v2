//
//  ProductCell.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *imageCV;
@property (weak, nonatomic) IBOutlet UIView *contentV;

@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *shortDescL;

@property (weak, nonatomic) IBOutlet UIView *priceInfoV;
@property (weak, nonatomic) IBOutlet UILabel *offerpriceL;
@property (weak, nonatomic) IBOutlet UILabel *originalpriceL;
@property (weak, nonatomic) IBOutlet UILabel *cashbackL;

@property (weak, nonatomic) IBOutlet UIButton *buyB;

- (void) configureDataForCell : (NSDictionary *) item;


@end
