//
//  FeaturedStylisCell.h
//  Ziva
//
//  Created by Leena on 27/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturedStylisCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;
@property (weak, nonatomic) IBOutlet UIView *imageCV;


@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

@property (weak, nonatomic) IBOutlet UIView *contentCV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *addressL;
@property (weak, nonatomic) IBOutlet UILabel *distanceL;

@property (weak, nonatomic) IBOutlet UIView *ratingsV;
@property (weak, nonatomic) IBOutlet UILabel *ratingsL;

@property (nonatomic) CGFloat lblMaxWidth;

- (void) configureDataForCell : (NSDictionary *) item;


@property (weak, nonatomic) IBOutlet UILabel *subtitleL;
@property (weak, nonatomic) IBOutlet UILabel *genderL;
@property (weak, nonatomic) IBOutlet UILabel *startingpriceL;
@property (weak, nonatomic) IBOutlet UILabel *locationL;


@end
