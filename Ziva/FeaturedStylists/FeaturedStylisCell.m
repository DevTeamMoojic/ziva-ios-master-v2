//
//  FeaturedStylisCell.m
//  Ziva
//
//  Created by Leena on 27/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "FeaturedStylisCell.h"

@implementation FeaturedStylisCell


- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
    
}

- (NSString *) getAddress : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData stringValueFromDictionary:item forKey:KEY_SALON_ADDRESS];
    return str;
}

- (NSString *) getStartingPriceDescription : (NSDictionary *) item{
    NSString *str = @"";
    str = [ReadData amountInRsFromDictionary:item forKey:KEY_STYLIST_STARTING_PRICE];
    str = [str stringByAppendingString:@" (Starting Price)"];
    
    if([[ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES] length] > 0 )
    {
        str = [str stringByAppendingString : @" for "];
        str = [str stringByAppendingString : [ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_SERVICES]];
    }
    return str;
}


- (void) configureDataForCell : (NSDictionary *) item
{
    ///
    self.mainImgV.image = Nil;
    self.mainImgV.alpha = 0;
    self.placeholderImgV.alpha=1;
    
    self.genderL.layer.borderWidth = 1;
    self.genderL.layer.borderColor = self.genderL.textColor.CGColor;
    
    self.titleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    self.subtitleL.text = [ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_PROFESSION];
    self.locationL.text = [ReadData stringValueFromDictionary:item forKey:KEY_STYLIST_LOCATION];
    self.startingpriceL.text = [self getStartingPriceDescription:item];
    
    self.genderL.text = [ReadData getGenderDescription:[ReadData genderIdFromDictionary:item forKey:KEY_GENDER]];
    
    CGFloat tmpWidth = [ResizeToFitContent getWidthForText:self.subtitleL.text usingFontType:FONT_SEMIBOLD FontOfSize:11 andMaxWidth:(CGRectGetWidth(self.titleL.frame) - 15 - CGRectGetWidth(self.genderL.frame))];
    self.subtitleL.frame = [UpdateFrame setSizeForView:self.subtitleL usingWidth: tmpWidth];
    self.genderL.frame = [UpdateFrame setPositionForView:self.genderL usingPositionX:(CGRectGetMaxX(self.subtitleL.frame) + 15)];
    
    CGFloat tmpHeight = [ResizeToFitContent getHeightForText:self.startingpriceL.text usingFontType:FONT_SEMIBOLD FontOfSize:11 andMaxWidth:self.lblMaxWidth];
    self.startingpriceL.frame = [UpdateFrame setSizeForView:self.startingpriceL usingHeight:tmpHeight];
    self.locationL.frame = [UpdateFrame setPositionForView:self.locationL usingPositionY:CGRectGetMaxY(self.startingpriceL.frame)];
    
    // ImageView
    NSString *url = [ReadData stringValueFromDictionary:item forKey:KEY_IMAGE_URL];
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL];
        [self.mainImgV setImageWithURLRequest:urlRequest
                             placeholderImage:Nil
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [self.mainImgV setImage:image];
             [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                   delay:0 options:UIViewAnimationOptionCurveLinear
                              animations:^(void)
              {
                  
                  [self.placeholderImgV setAlpha:0 ];
                  [self.mainImgV setAlpha:1 ];
              }
                              completion:^(BOOL finished)
              {
              }];
             
             
             
         }
                                      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
