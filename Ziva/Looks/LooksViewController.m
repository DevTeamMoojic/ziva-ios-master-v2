//
//  LooksViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "LookCell.h"
#import "GenderSelectorViewController.h"
#import "LookDetailViewController.h"
#import "LooksViewController.h"

#define ROW_HEIGHT 260

@interface LooksViewController ()<APIDelegate,MBProgressHUDDelegate,GenderSelectorDelegate>
{
    MBProgressHUD *HUD;
    
    NSMutableArray *looksArray;
    
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIView *genderselectorCV;
    __weak GenderSelectorViewController *genderselectorVC;

    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    __weak IBOutlet UIView *backV;
    
    NSString *selectedGenderId;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    
    AppDelegate *appD;
    
    CGFloat contentHeight;
}
@end

@implementation LooksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else{
        isBackFromOtherView = NO;
    }
}

#pragma mark - Public Methods

- (void) loadLooks{
    if ([InternetCheck isOnline])
    {
        noresultsL.hidden = YES;
        listTblV.hidden = YES;
        
        [self hudShowWithMessage:@"Loading"];
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_LOOKS Param:nil];
    }
}

- (void) showLook_DetailView : (NSDictionary *) item{
    [self performSegueWithIdentifier:@"showlookdetail" sender:item];
}

#pragma mark - Methods

- (CGFloat) calculateImageHeight
{
    CGFloat designScreenHeight = 667;
    CGFloat designHeight = 230;
    
    CGFloat translatedHeight = ceil(designHeight * CGRectGetHeight(self.view.frame)/designScreenHeight);
    
    return (int)translatedHeight;
}


- (void) setupLayout
{
    looksArray = [NSMutableArray array];
    
    selectedGenderId = [appD getLoggedInUserGender];
    [genderselectorVC setGender:selectedGenderId];
    
    pageWidth = CGRectGetWidth(self.view.bounds);

    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [listTblV removeGestureRecognizer:listTblV.panGestureRecognizer];
    
    //Position elements
    {
        
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        genderselectorCV.frame = [UpdateFrame setPositionForView:genderselectorCV usingPositionY:CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(genderselectorCV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(genderselectorCV.frame)];
    }
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self loadLooks];
}

-(CGFloat) calculateHeightOfList
{
    return ([looksArray count]  * ROW_HEIGHT);
}

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

#pragma mark - Process Response

- (void) processResponse_Looks : (NSArray *)arr
{
    if([looksArray count] > 0) [looksArray removeAllObjects];
    for(NSDictionary *item in arr)
    {   NSArray *looks = [ReadData arrayFromDictionary:item forKey:KEY_LOOKCATEGORY_LOOKS];
        for(NSDictionary *lookDict in looks)
        if([[ReadData stringValueFromDictionary:lookDict forKey:KEY_GENDER] isEqualToString:selectedGenderId]){
            [looksArray addObject:lookDict];
        }
    }
    [listTblV reloadData];
    
    noresultsL.hidden = ([looksArray count] > 0);
    listTblV.hidden = ([looksArray count] == 0);
    
    contentHeight = [self calculateHeightOfList];
    if(contentHeight <= CGRectGetHeight(tabcontentscrollView.bounds)){
        contentHeight = CGRectGetHeight(tabcontentscrollView.bounds);
        listTblV.scrollEnabled = NO;
    }
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightOfList]];
    [self tabChanged];
}


#pragma mark - Gender Selector & Delegate Methods

- (void) valueChanged{
    selectedGenderId = [genderselectorVC getGender];
    [self loadLooks];
}

#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}


#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(genderselectorCV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    listTblV.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [listTblV contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return ROW_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [looksArray count] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LookCell *cell = (LookCell *)[tableView dequeueReusableCellWithIdentifier:@"lookcell" forIndexPath:indexPath];
    NSDictionary *item = looksArray[indexPath.row];
    [cell configureDataForCell :item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    isBackFromOtherView = YES;
    NSDictionary *item = looksArray[indexPath.row];
    [self showLook_DetailView:item];
}


#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}

#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //showgenderselector_services
    if([segue.identifier isEqualToString:@"showgenderselector_looks"]){
        genderselectorVC = (GenderSelectorViewController *)segue.destinationViewController;
        genderselectorVC.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"showlookdetail"]){
        
        isBackFromOtherView = YES;
        LookDetailViewController *detailVC = (LookDetailViewController *)segue.destinationViewController;
        [detailVC loadDetailView:sender];
    }
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    [HUD hide:YES];
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_LOOKS]) {
        [self processResponse_Looks:[ReadData arrayFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
}

@end
