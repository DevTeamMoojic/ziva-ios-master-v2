//
//  LookCategoryListViewController.h
//  Ziva
//
//  Created by Bharat on 23/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LookCategoryListViewController : UIViewController

- (void) loadData: (NSArray *) listArray  withCellContentSize : (CGSize) contentsize;

- (CGFloat) getContentHeight;
- (void) scrollContent : (CGPoint) offset;
- (CGPoint) getScrollContent ;

@end
