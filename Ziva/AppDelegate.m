//
//  AppDelegate.m
//  Ziva
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Smooch/Smooch.h>

#import "UpgradeAppViewController.h"
#import "RootViewController.h"
#import "ChooseGenderViewController.h"
#import "SignInViewController.h"
#import "SlideShowViewController.h"
#import "SplashViewController.h"
#import "AppDelegate.h"

@interface AppDelegate ()<CLLocationManagerDelegate,APIDelegate>
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *currentAddress;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.sessionDelegate = [self sessionDelegate];
    
    //Customize Smooch SDK
    {
        SKTSettings* settings = [SKTSettings settingsWithAppToken:@"323hmfsbeecssvvgx68cod565 "];
        settings.conversationAccentColor = [UIColor brandOrangeDisplayColor];
        
        [[UINavigationBar appearance] setBarTintColor:[UIColor brandPurpleDisplayColor]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont regularFontOfSize:19],NSForegroundColorAttributeName : [UIColor whiteColor] }];
    
        [Smooch initWithSettings:settings];
    }
    
    [Fabric with:@[CrashlyticsKit]];
    
    return YES;
}


- (SessionDelegate *)sessionDelegate
{
    static SessionDelegate *delegate = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        // session delegate
        delegate = [SessionDelegate new];
    });
    return delegate;
}


#pragma mark - Application Events

- (void)registerForRemoteNotification {
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound  categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        //[[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

- (BOOL)checkNotificationType:(UIUserNotificationType)type
{
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    return (currentSettings.types & type);
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [self sendDeviceTokenInfo:newToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    NSInteger badgeNumber = 0;
    
    if ([self checkNotificationType:UIUserNotificationTypeBadge])
    {
        application.applicationIconBadgeNumber = badgeNumber;
    }
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
}

#ifdef __IPHONE_9_0

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    
    return YES;
}

#endif

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    
    return YES;
}


#pragma mark - Location Management

- (void)startLocationServices
{
    //for getting location of user
    if (!locationManager)
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate   = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = 5;
    }
    
    if (!geocoder) {
        geocoder = [[CLGeocoder alloc] init];
    }
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    if ([error domain] == kCLErrorDomain) {
        
        // We handle CoreLocation-related errors here
        switch ([error code]) {
                // "Don't Allow" on two successive app launches is the same as saying "never allow". The user
                // can reset this for all apps by going to Settings > General > Reset > Reset Location Warnings.
            case kCLErrorDenied:
                [locationManager stopUpdatingLocation];
                break;
                
            case kCLErrorLocationUnknown:
                [locationManager stopUpdatingLocation];
                break;
                
            default:
                break;
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied) {
        // permission denied
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways) {
        // permission granted
    }
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // permission granted
    }    
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations lastObject];
    
    if (currentLocation)
    {
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil && [placemarks count] > 0)
            {
                placemark = [placemarks lastObject];
                [self locationUpdated];
                
            } else {
                NSLog(@"%@", error.debugDescription);
            }
        } ];
    }
}

- (CLLocation *) getUserCurrentLocation {
    //currentLocation = [[CLLocation alloc] initWithLatitude: 19.186627 longitude:72.8326446];
    return currentLocation;
}

- (NSString *) getUserCurrentLocationLatitude {
    //return [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    return @"19.1512"; // @"19.186627";
}

- (NSString *) getUserCurrentLocationLongitude {
    //return [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    return @"72.832263"; // @"72.8326446";
}

- (NSString *) getUserCurrentLocationAddress {
    NSMutableArray *addressFields = [NSMutableArray array];
    
    if (placemark.thoroughfare) [addressFields addObject:placemark.thoroughfare];
    if (placemark.subThoroughfare) [addressFields addObject:placemark.subThoroughfare];
    if (placemark.locality) [addressFields addObject:placemark.locality];
    if (placemark.postalCode) [addressFields addObject:placemark.postalCode];
    if (placemark.administrativeArea) [addressFields addObject:placemark.administrativeArea];
    if (placemark.country) [addressFields addObject:placemark.country];
    return [addressFields componentsJoinedByString:@", "];
}

#pragma mark - Methods

- (BOOL)isFirstRun
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef boolForKey:ISFIRSTRUN];
}

- (BOOL)isVersionUpgrade
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    NSString *versionNo =  [standardDef valueForKey:PARAM_APP_VERSION];
    if (!versionNo) versionNo = @"";
    return ![versionNo isEqualToString:[self getAppVersionNo]];
}

- (BOOL)isLoggedIn
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    NSString *environment = [standardDef valueForKey:PARAM_APP_ENVIRONMENT];
    if (!environment) environment = @"";
    return ([environment isEqualToString:[self getAppEnvironment]] && [standardDef boolForKey:ISUSERLOGGEDIN]);
}

- (NSString *) getAppEnvironment
{
    NSString *apiDomain = [[[PlistHelper new] getDictionaryForPlist:@"APIs"] objectForKey:@"apiDomain"];
    NSString *isDevBuild = ENVIRONMENT_PRODUCTION;
    if ([apiDomain rangeOfString:@"zivadiva"].location != NSNotFound) {
        isDevBuild = ENVIRONMENT_DEV;
    }
    return isDevBuild;
}

- (NSString *) getAppVersionNo
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

- (NSString *) getLoggedInUserId {
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_REGISTRATION_ID];
}

- (NSString *) getLoggedInUserGender {
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_GENDER];    
}

- (NSString *) getLoggedInUserDisplayName {
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [NSString stringWithFormat:@"%@ %@",[standardDef stringForKey:USER_FIRSTNAME],[standardDef stringForKey:USER_LASTNAME]];
}

- (NSString *) getLoggedInUserFirstName {
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_FIRSTNAME];
}

- (NSString *) getLoggedInUserLastName {
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_LASTNAME];
}

- (NSString *) getLoggedInUserMobileNumber{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_MOBILE];
}

- (NSString *) getLoggedInUserEmailAddress{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_EMAIL];
}

- (NSString *) getLoggedInUserProfileURL{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_PROFILE_URL];
}

- (NSString *) getLoggedInUserBirthDate{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    return [standardDef stringForKey:USER_BIRTHDATE];
}

- (BOOL) isProfileCompleted
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    BOOL isComplete = YES;
    if([self isEmptyString: [standardDef stringForKey:USER_FIRSTNAME]] || [self isEmptyString: [standardDef stringForKey:USER_LASTNAME]] || [self isEmptyString: [standardDef stringForKey:USER_EMAIL]])
    {
        isComplete =  NO;
    }
    return isComplete;
}

- (void) pushProfile : (BOOL) newSesion
{
    [self.sessionDelegate loadDataForLoggedInUser ];    
    
//    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
//    
//    NSString *full_name = [standardDef valueForKey:PARAM_FULL_NAME];
//    if ([full_name isEqual:[NSNull null]]) full_name = @"";
//    
//    NSString *email = [standardDef valueForKey:PARAM_EMAIL];
//    if ([email isEqual:[NSNull null]]) email = @"";
//    
//    NSString *mobile = [standardDef valueForKey:KEY_USERMOBILE_NO];
//    if ([mobile isEqual:[NSNull null]]) mobile = @"";
//    
//    if (newSesion)
//    {
//        
//        NSMutableDictionary *profile = [NSMutableDictionary dictionary];
//        [profile setValue:email forKey:@"Identity"];
//        [profile setValue:full_name forKey:@"Name"];
//        [profile setValue:email forKey:@"Email"];
//        [profile setValue:mobile forKey:@"Phone"];
//        [profile setValue:[self preferredCityName] forKey:@"City"];
//        
//    }
//    [CrashlyticsKit setUserName:full_name];
//    [CrashlyticsKit setUserEmail:email];
}



- (void)loginUser
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    [standardDef setBool:YES forKey:ISUSERLOGGEDIN];
    //[standardDef setBool:YES forKey:ISFIRSTRUN];
    [standardDef synchronize];
    
    [self registerForRemoteNotification];
    [self showHomeVC:YES];

    
}

- (void)logoutUser
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    [standardDef setBool:NO forKey:ISUSERLOGGEDIN];
    [standardDef synchronize];
    
    [self showSlideshowVC];
}

-(void) sendDeviceTokenInfo : (NSString *) token
{
    if ([InternetCheck isOnline])
    {
        //CustomerId, Device Token, Platform (0 - Android, 1 - iPhone, 2 - Windows Phone)
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:[self getLoggedInUserId] forKey:KEY_CUSTOMER_ID];
        [paramDict setObject: token forKey:@"Token"];
        [paramDict setObject: @"1" forKey:PARAM_PLATFORM];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_REGISTER_DEVICE  Param: paramDict];
    }
}

- (void)showOfflineAlert
{
    UIAlertView *networkAlert = [[UIAlertView alloc] initWithTitle:@"Device is Offline" message:NETWORKERROR delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [networkAlert show];
}

-(NSArray *)excludeShareItems
{
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    return excludeActivities;
}

#pragma mark - Notify Profile & Location Update

- (void) profileUpdated{
    UIViewController *controller = self.window.rootViewController  ;
    if ([controller isKindOfClass:[RootViewController class]])
    {
        RootViewController *rootVC = (RootViewController *)controller;
        [rootVC profileUpdated];
    }
}

- (void) locationUpdated{
    UIViewController *controller = self.window.rootViewController  ;
    if ([controller isKindOfClass:[RootViewController class]])
    {
        RootViewController *rootVC = (RootViewController *)controller;
        [rootVC locationUpdated];
    }
}

- (void) backToHome {
    UIViewController *controller = self.window.rootViewController  ;
    if ([controller isKindOfClass:[RootViewController class]])
    {
        RootViewController *rootVC = (RootViewController *)controller;
        [rootVC backToHome];
    }    
}


#pragma mark - Reload listing pages


#pragma mark - Show VCs

- (void)showSlideshowVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SlideShowViewController *splashVC = (SlideShowViewController *)[mainStoryboard
                                                              instantiateViewControllerWithIdentifier:@"slideshowVC"];
    self.window.rootViewController = splashVC;
}


- (void)showSigninVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SignInViewController *signinVC = (SignInViewController *)[mainStoryboard
                                                              instantiateViewControllerWithIdentifier:@"signinVC"];
    self.window.rootViewController = signinVC;
}


- (void)showHomeVC : (BOOL) newsSession
{
    [self pushProfile: newsSession];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    RootViewController *homeVC = (RootViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"rootController"];
    self.window.rootViewController = homeVC;
}

- (void) showChooseGenderVC
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    ChooseGenderViewController *genderVC = (ChooseGenderViewController *)[mainStoryboard
                                                              instantiateViewControllerWithIdentifier:@"choosegenderVC"];
    self.window.rootViewController = genderVC;
    
}

- (void)forceAppUpdate
{
    UIViewController *controller = [self topViewController];
    
    if (![controller isKindOfClass:[UpgradeAppViewController class]])
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        UpgradeAppViewController *upgradeappVC = (UpgradeAppViewController *)[mainStoryboard
                                                                              instantiateViewControllerWithIdentifier:@"showappupgrade"];
        
        [controller presentViewController:upgradeappVC animated:NO completion:Nil];
        
    }
}

- (void) showAPIErrorWithMessage : (NSString *) message andTitle : (NSString *) title
{
    if ([message isEqualToString:@"Please login into your Ziva account."])
    {
        [self logoutUser];
        [self showSigninVC];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark - Validation Rules

- (BOOL) isEmptyString : (NSString *) str
{
    return ([str isEqualToString:@""] || ([[str stringByReplacingOccurrencesOfString:@" " withString:@""] length]) == 0);
}

-(BOOL) NSStringIsValidEmail:(NSString *)str {
    BOOL stricterFilter = YES;
    // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:str];
}

-(BOOL) NSStringIsValidMobile:(NSString *)str {
    NSString *mobileRegex = @"^(\\+?)(\\d{10,})$";
    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    return [mobileTest evaluateWithObject:str];
}


#pragma mark - Chat Support

- (void) startChatSupport : (NSString *) message
{
    NSString *email = [self getLoggedInUserEmailAddress];
    NSString *fullName = [self getLoggedInUserDisplayName];
    NSString *contactNo = [self getLoggedInUserMobileNumber];
    
    if (![self isEmptyString:fullName])
    [SKTUser currentUser].firstName = fullName;
    if (![self isEmptyString:email]){
        [SKTUser currentUser].email = email;
    }
    if (![self isEmptyString:contactNo])
    [[SKTUser currentUser] addProperties:@{ @"mobile" : contactNo}];
    
    //[[Smooch conversation] sendMessage:[[SKTMessage alloc] initWithText:message]];
    [Smooch show];
}

- (void) startSupportCall
{
    NSString *phoneNumber = [NSString stringWithFormat:@"telprompt://+918082180821"];
    NSString *escaped = [phoneNumber stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"CAll Us");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:escaped]];

}

#pragma mark - Topmost Controller

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

#pragma mark - Tracking SDKS

- (void) recordEvent : (NSString *) eventName withProperties : (NSDictionary *)dict
{
}

#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
}

@end
