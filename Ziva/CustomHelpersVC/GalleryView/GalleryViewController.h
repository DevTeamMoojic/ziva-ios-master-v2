//
//  GalleryViewController.h
//  Ziva
//
//  Created by Bharat on 27/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController

- (void) setImageForFullScreen : (UIImage *) image;
//- (void)setupScrollView:(UIScrollView*)scrMain ;
@end
