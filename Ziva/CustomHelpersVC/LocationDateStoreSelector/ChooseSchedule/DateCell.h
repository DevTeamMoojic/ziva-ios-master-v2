//
//  DateCell.h
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UILabel *dayPartL;
@property (weak, nonatomic) IBOutlet UILabel *datePartL;

@property (weak, nonatomic) IBOutlet UIButton *dateSelectedB;

- (void) configureDataForCell : (NSDictionary *) item usingFormatter : (NSDateFormatter *) formatter;

- (void) itemSelected : (BOOL) selected ;

@end
