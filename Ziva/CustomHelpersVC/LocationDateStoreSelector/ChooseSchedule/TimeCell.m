//
//  TimeCell.m
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "TimeCell.h"

@implementation TimeCell

- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}

- (void) itemSelected : (BOOL) selected  {
    UIColor *backSelectedColor = [UIColor colorWithRed:244.0/255.0 green:170.0/255.0 blue:16.0/255.0 alpha:1.0];
    UIColor *selectedColor = [UIColor whiteColor];
    UIColor *notselectedColor= [UIColor borderDisplayColor];
    
    self.slotSelectedB.selected = selected;
    self.timeV.backgroundColor = selected ? backSelectedColor : [UIColor clearColor];
    self.slotDescL.textColor = selected ? selectedColor : notselectedColor;
}

- (void) configureDataForCell : (NSDictionary *) item
{
    self.mainV.layer.borderWidth = 0.5f;
    self.mainV.layer.borderColor = [UIColor borderDisplayColor].CGColor ;
    
    self.slotDescL.text = [NSString stringWithFormat:@"%@ - %@",[ReadData stringValueFromDictionary:item forKey:KEY_SLOT_START_TIME],[ReadData stringValueFromDictionary:item forKey:KEY_SLOT_END_TIME]];
    
    self.slotSelectedB.selected = NO;
}

@end
