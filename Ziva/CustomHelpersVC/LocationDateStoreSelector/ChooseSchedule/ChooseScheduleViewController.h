//
//  ChooseScheduleViewController.h
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChooseScheduleDelegate <NSObject>

- (void) oncancel_Schedule;

- (void) onsuccess_Schedule;

@end


@interface ChooseScheduleViewController : UIViewController

@property (nonatomic, weak) id<ChooseScheduleDelegate> delegate;

- (void) updateBackFromOtherViewStatus : (BOOL) status;

@end
