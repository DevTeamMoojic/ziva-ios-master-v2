//
//  ChooseLocationViewController.m
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ChooseLocationViewController.h"

@interface ChooseLocationViewController (){

    AppDelegate *appD;
    MyCartController *cartC;
    
    BOOL isBackFromOtherView;
}
@end

@implementation ChooseLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;

    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isBackFromOtherView){
        
    }
    else{
        isBackFromOtherView = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!isBackFromOtherView){
        

    }
}

#pragma mark - Public Methods

- (void) updateBackFromOtherViewStatus : (BOOL) status{
    isBackFromOtherView = status;
}

#pragma mark - Methods

- (void) setupLayout
{
    
}


@end
