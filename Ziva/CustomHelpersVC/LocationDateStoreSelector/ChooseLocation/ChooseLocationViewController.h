//
//  ChooseLocationViewController.h
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChooseLocationDelegate <NSObject>

- (void) oncancel_Location;

- (void) onsuccess_Location;

@end

@interface ChooseLocationViewController : UIViewController

@property (nonatomic, weak) id<ChooseLocationDelegate> delegate;

- (void) updateBackFromOtherViewStatus : (BOOL) status;

@end
