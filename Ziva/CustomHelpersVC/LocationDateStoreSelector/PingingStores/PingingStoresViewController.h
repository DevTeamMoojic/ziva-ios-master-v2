//
//  PingingStoresViewController.h
//  Ziva
//
//  Created by Bharat on 28/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PingingStoresDelegate <NSObject>

@optional

- (void) oncancel_Store;

- (void) onsuccess_Store;

@end

@interface PingingStoresViewController : UIViewController

@property (nonatomic, weak) id<PingingStoresDelegate> delegate;

- (void) activatePinging;

- (void) updateBackFromOtherViewStatus : (BOOL) status;

@end
