//
//  GenderSelectorViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GenderSelectorDelegate <NSObject>

- (void) valueChanged;

@end

@interface GenderSelectorViewController : UIViewController

@property (nonatomic, weak) id<GenderSelectorDelegate> delegate;

- (NSString *) getGender;
- (void) setGender : (NSString *) genderId;

@end
