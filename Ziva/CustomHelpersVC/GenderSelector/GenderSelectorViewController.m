//
//  GenderSelectorViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "GenderSelectorViewController.h"

@interface GenderSelectorViewController (){
 
    __weak IBOutlet UIButton *femaleB;
    __weak IBOutlet UIButton *maleB;
    __weak IBOutlet UISwitch *genderSwitchV;
    
    NSString *selectedGenderId;
}
@end

@implementation GenderSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    maleB.selected = YES;
    femaleB.selected = NO;
    [genderSwitchV setOn:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

-(NSString *)getGender{
    return selectedGenderId;
}

- (void) setGender : (NSString *) genderId{
    selectedGenderId = genderId;
    [self updateButons];
}

- (void) updateButons{
    if([selectedGenderId isEqualToString:GENDER_FEMALE]){
        femaleB.selected = YES;
        maleB.selected = NO;
        [genderSwitchV setOn:YES];
    }
    else if([selectedGenderId isEqualToString:GENDER_MALE]){
        femaleB.selected = NO;
        maleB.selected = YES;
        [genderSwitchV setOn:NO];
    }
}

#pragma mark - Events

-(IBAction)optionBPressed:(UIButton *)sender{
    if(sender.selected) return;
    if([sender isEqual:femaleB]){
        selectedGenderId = GENDER_FEMALE;
    }
    else if([sender isEqual:maleB]){
        selectedGenderId = GENDER_MALE;
    }
    [self updateButons];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(valueChanged)])
    {
        [self.delegate valueChanged] ;
    }
}

- (IBAction)switchChanged:(UISwitch *)sender {
    if(sender.isOn){
        selectedGenderId = GENDER_FEMALE;
    }
    else{
        selectedGenderId = GENDER_MALE;
    }
    [self updateButons];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(valueChanged)])
    {
        [self.delegate valueChanged] ;
    }    
}

@end
