//
//  OrderLineItemsViewController.m
//  Ziva
//
//  Created by Bharat on 12/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "OrderLineItemCell.h"
#import "OrderLineItemsViewController.h"

@interface OrderLineItemsViewController ()
{
    NSArray *lineItems;
    
    __weak IBOutlet UILabel *headingL;
    __weak IBOutlet UITableView *listTblV ;
}
@end

@implementation OrderLineItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) loadData : (NSArray *) dataArray withHeading :(NSString *) cartType ;
{
    lineItems = dataArray;
    headingL.text = [self getHeadingDescription:cartType];
    [listTblV reloadData];
}

#pragma mark - Methods

- (NSString *) getHeadingDescription : (NSString *) cartType
{
    NSString *val = @"";
    if([cartType isEqualToString:CART_TYPE_SERVICES]){
        val = @"Services";
    }
    else if([cartType isEqualToString:CART_TYPE_PRODUCTS]){
        val = @"Products";
    }
    else if([cartType isEqualToString:CART_TYPE_LOOKS]){
        val = @"Looks";
    }
    else if([cartType isEqualToString:CART_TYPE_PACKAGES]){
        val = @"Packages";
    }
    return val;
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    return 46.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [lineItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderLineItemCell *cell = (OrderLineItemCell *)[tableView dequeueReusableCellWithIdentifier:@"orderlineitemcell" forIndexPath:indexPath];
    NSDictionary *item = lineItems[indexPath.row];
    [cell configureDataForCell :item];
    
    return cell;
}



@end
