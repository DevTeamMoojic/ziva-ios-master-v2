//
//  OrderLineItemCell.m
//  Ziva
//
//  Created by Bharat on 12/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "OrderLineItemCell.h"

@implementation OrderLineItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [super awakeFromNib];
    
    // Fix : Autoresizing issues for iOS 7 using Xcode 6
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //    Fix : For right aligned view with autoresizing
    UIView *backV = [[self.contentView subviews] objectAtIndex:0];
    backV.frame = [UpdateFrame setSizeForView:backV usingSize:self.bounds.size];
}


- (void) configureDataForCell : (NSDictionary *) item
{
    NSString *title = [ReadData stringValueFromDictionary:item forKey:KEY_NAME];
    NSInteger quantity = [ReadData integerValueFromDictionary:item forKey:KEY_QUANTITY];
    
    self.lineitemDescL.text = title;
    self.lineitemQuantityL.text = [NSString stringWithFormat:@"%ld",(long) quantity];
    self.lineitemAmountL.text = [ReadData amountInRsFromDictionary:item forKey:KEY_AMOUNT];
    
}

@end
