//
//  DatePickerViewController.h
//  Ziva
//
//  Created by Bharat on 17/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

@protocol DatePickerDelegate <NSObject>

- (void) dateSelected ;
- (void) hideDatePicker;

@end


@interface DatePickerViewController : UIViewController


@property (nonatomic, weak) id<DatePickerDelegate> delegate;

- (NSDate *) selectedDate ;

- (void) setMinimumDate :(NSDate  *) value;

- (void) setMaximumDate :(NSDate  *) value;

- (void) setDate :(NSDate  *) value;

@end

