//
//  MySearchBarViewController.m
//  Ziva
//
//  Created by Bharat on 27/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//


#import "MySearchBarViewController.h"

@interface MySearchBarViewController ()<UISearchBarDelegate>
{
    __weak IBOutlet UISearchBar  *searchBar;

}
@end

@implementation MySearchBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSearchBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) initSearchBar
{
    searchBar.text = @"";
}

- (void) startSearching
{
    [searchBar becomeFirstResponder];
    searchBar.showsCancelButton = YES;
}

#pragma mark - Private Method

- (void) setupSearchBar
{
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    
    txfSearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    txfSearchField.font = [UIFont semiboldFontOfSize:12.0];
    txfSearchField.textColor = [UIColor searchTextDisplayColor];
    txfSearchField.tintColor = txfSearchField.textColor;
    txfSearchField.placeholder = @"Search Services";
    
    //To remove black border around searchbar
    [searchBar setBackgroundImage:[[UIImage alloc]init]];
    for (UIView * view in [[[searchBar subviews] objectAtIndex:0] subviews])
    {
        if (![view isKindOfClass:[UITextField class]] && ![view isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
        {
            view .alpha = 0;
        }
    }
}

#pragma mark - Searchbar delegate methods

- (void) searchBarSearchButtonClicked:(UISearchBar *)asearchBar
{
    [searchBar resignFirstResponder];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(searchTextChanged:)])
    {
        [self.delegate searchTextChanged : asearchBar.text] ;
    }    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)asearchBar
{
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(searchCancelled)])
    {
        [self.delegate searchCancelled] ;
    }
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)asearchBar
{
    [self startSearching];
}

- (void)searchBar:(UISearchBar *)asearchBar textDidChange:(NSString *)searchText
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(searchTextChanged:)])
    {
        [self.delegate searchTextChanged : searchText] ;
    }
    
}

@end
