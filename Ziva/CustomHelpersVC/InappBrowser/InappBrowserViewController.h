//
//  InappBrowserViewController.h
//  Ziva
//
//  Created by Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InappBrowserViewController : UIViewController

- (void) loadURL : (NSURL *) url;

@end
