//
//  MakeHTMLString.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "MakeHTMLString.h"

@implementation MakeHTMLString

+ (NSAttributedString *)getHTMLString : (NSString *) html FontFamily :(NSString *) family FontSize: (CGFloat) size
{
    NSString *htmlString = [NSString stringWithFormat:
                            @"<html><head>"
                            "<style type=\"text/css\">"
                            "body{"
                            "color:#575757;\n"
                            "font-family: \"%@\";font-size:%.1fpx;\n"
                            "}"
                            "</style>"
                            "</head>\n"
                            "<body>\n"
                            "%@"
                            "</body>\n"
                            "</html>\n",
                            family,size,html];
    
    return [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

}

+ (NSAttributedString *)getHTMLString : (NSString *) html
{
    return [self getHTMLString:html FontFamily:@"Lato-Light" FontSize:14];
}

+ (NSAttributedString *) getHTMLString : (NSString *) html FontSize: (CGFloat) size
{
    return [self getHTMLString:html FontFamily:@"Lato-Light" FontSize:size];
}

+ (NSAttributedString *)getCenteredHTMLString : (NSString *) html FontFamily :(NSString *) family FontSize: (CGFloat) size
{
    NSString *htmlString = [NSString stringWithFormat:
                            @"<html><head>"
                            "<style type=\"text/css\">"
                            "body{"
                            "text-align:center; \n"
                            "color:#575757;\n"
                            "font-family: \"%@\";font-size:%.1fpx;\n"
                            "}"
                            "</style>"
                            "</head>\n"
                            "<body>\n"
                            "%@"
                            "</body>\n"
                            "</html>\n",
                            family,size,html];
    
    return [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
}

@end
