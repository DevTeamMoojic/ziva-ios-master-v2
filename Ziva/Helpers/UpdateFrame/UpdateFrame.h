//
//  UpdateFrame.h
//  Ziva
//
//  Created by Bharat on 22/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdateFrame : NSObject

+ (CGRect)setPositionForView:(UIView *) sourceView usingPosition : (CGPoint) transformedPoint;

+ (CGRect)setPositionForView:(UIView *) sourceView usingPositionX : (CGFloat) valueX andPositionY : (CGFloat) valueY;

+ (CGRect)setPositionForView:(UIView *) sourceView usingPositionX : (CGFloat) valueX;

+ (CGRect)setPositionForView:(UIView *) sourceView usingPositionY : (CGFloat) valueY;


+ (CGRect)setPositionForView:(UIView *) sourceView usingOffsetX : (CGFloat) valueX andOffsetY : (CGFloat) valueY;

+ (CGRect)setPositionForView:(UIView *) sourceView usingOffsetX : (CGFloat) valueX;

+ (CGRect)setPositionForView:(UIView *) sourceView usingOffsetY : (CGFloat) valueY;


+ (CGRect)setSizeForView:(UIView *) sourceView usingSize : (CGSize) transformedSize;

+ (CGRect)setSizeForView:(UIView *) sourceView usingWidth : (CGFloat) transformedWidth andHeight : (CGFloat) transformedHeight;

+ (CGRect)setSizeForView:(UIView *) sourceView usingWidth : (CGFloat) transformedWidth ;

+ (CGRect)setSizeForView:(UIView *) sourceView usingHeight : (CGFloat) transformedHeight;


@end
