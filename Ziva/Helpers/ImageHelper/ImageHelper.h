//
//  ImageHelper.h
//  Ziva
//
//  Created by Bharat on 06/03/16.
//  Copyright © 2016 Ziva Lifestyle India Private Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageHelper : NSObject

+ (NSString *)imageToNSString:(UIImage *)image;

+ (UIImage *)stringToUIImage:(NSString *)string;

+ (UIImage *)resizeImage:(UIImage *)image;

+ (UIImage *)grayScaleImage:(UIImage *)image;
@end
