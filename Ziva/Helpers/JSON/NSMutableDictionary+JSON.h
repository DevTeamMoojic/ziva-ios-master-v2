//  NSMutableDictionary+JSON.h
//   
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (JSON)

+ (NSMutableDictionary *)dictionaryWithContentsOfJSONURLString: (NSString* )urlAddress;
- (BOOL)isConvertibleToJSON;
- (NSData *)toJSON;

@end
