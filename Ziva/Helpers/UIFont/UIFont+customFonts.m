//  UIFont+customFonts.m
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import "UIFont+customFonts.h"

@implementation UIFont (customFonts)


+ (UIFont *) getFontType : (NSString *)type andSize : (CGFloat) size{
    UIFont *customFont ;
    if([type isEqualToString:FONT_REGULAR]) {
        customFont =  [UIFont regularFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_REGULAR]) {
        customFont =  [UIFont italicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_BOLD]) {
        customFont =  [UIFont boldFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_BOLD]) {
        customFont =  [UIFont boldItalicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_LIGHT]) {
        customFont =  [UIFont lightFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_LIGHT]) {
        customFont =  [UIFont lightItalicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_BLACK]) {
        customFont =  [UIFont blackFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_BLACK]) {
        customFont =  [UIFont blackItalicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_HAIRLINE]) {
        customFont =  [UIFont hairlineFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_HAIRLINE  ]) {
        customFont =  [UIFont hairlineItalicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_SEMIBOLD]) {
        customFont =  [UIFont semiboldFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_SEMIBOLD  ]) {
        customFont =  [UIFont semiboldItalicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_MEDIUM]) {
        customFont =  [UIFont mediumFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_MEDIUM  ]) {
        customFont =  [UIFont mediumItalicFontOfSize:size];
    }
    else if([type isEqualToString:FONT_HEAVY]) {
        customFont =  [UIFont heavyFontOfSize:size];
    }
    else if([type isEqualToString:FONT_ITALIC_HEAVY  ]) {
        customFont =  [UIFont heavyItalicFontOfSize:size];
    }
    return  customFont;
}

+ (UIFont *) regularFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Regular" size:size];
    
    return customFont;
}

+ (UIFont *) blackFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Black" size:size];
    
    return customFont;
}

+ (UIFont *) boldFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Bold" size:size];
    
    return customFont;
}

+ (UIFont *) hairlineFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Hairline" size:size];
    
    return customFont;
}

+ (UIFont *) lightFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Light" size:size];
    
    return customFont;
}

+ (UIFont *) semiboldFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Semibold" size:size];
    
    return customFont;
}
+ (UIFont *) heavyFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Heavy" size:size];
    
    return customFont;
}
+ (UIFont *) mediumFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Medium" size:size];
    
    return customFont;
}

+ (UIFont *) italicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-Italic" size:size];
    
    return customFont;
}

+ (UIFont *) blackItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-BlackItalic" size:size];
    
    return customFont;
}

+ (UIFont *) boldItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-BoldItalic" size:size];
    
    return customFont;
}

+ (UIFont *) hairlineItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-HairlineItalic" size:size];
    
    return customFont;
}

+ (UIFont *) lightItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-LightItalic" size:size];
    
    return customFont;
}

+ (UIFont *) semiboldItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-SemiboldItalic" size:size];
    
    return customFont;
}
+ (UIFont *) heavyItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-HeavyItalic" size:size];
    
    return customFont;
}
+ (UIFont *) mediumItalicFontOfSize: (CGFloat) size
{
    UIFont *customFont = [UIFont fontWithName:@"Lato-MediumIttalic" size:size];
    
    return customFont;
}

@end
