//
//  SplashViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()
{
    AppDelegate *appD;
}
@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appD = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    [self performSelector:@selector(showNextView) withObject:nil afterDelay:0.3];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)showNextView
{
    
    {
        BOOL isLoggedIn =  [appD isLoggedIn];
        
        if (isLoggedIn)
        {            
            [self showLandingView];
        }
        else
        {
            [self showLogin];
        }
    }
}

- (void) forceUserToUpdateApp
{
    [appD forceAppUpdate];
}

- (void)showLandingView
{
    [appD registerForRemoteNotification];
    if ([[appD getLoggedInUserGender] isEqualToString:@""]) {
        [appD showChooseGenderVC];
    }
    else
    {
        [appD showHomeVC:NO];
    }
}

- (void)showLogin
{
    [appD showSlideshowVC];
}

- (void)showOfflineAlert
{
    UIAlertView *networkAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:NETWORKERROR delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [networkAlert show];
}

@end
