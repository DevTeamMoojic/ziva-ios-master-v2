//
//  MyTrackingController.m
//  Ziva
//
//  Created by Bharat on 13/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "MyTrackingController.h"

#define TRACKING_INFO @"TrackingInfo"

#define KEY_TRACK_ELEMENT_LOOKS @"Looks"

#define KEY_ACCESSED_DTM @"AccessedDTM"
#define KEY_ACCESSED_FREQUENCY @"AccessedFrequency"

@interface MyTrackingController ()
{
    AppDelegate *appD;    
    NSMutableDictionary *trackingDict;
    
    NSDateFormatter *formatter;
    
}
@end

@implementation MyTrackingController

- (id)init
{
    if (self = [super init])
    {
        [self initData];
    }
    return self;
}

#pragma mark - Private Methods

- (void)initData
{
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    formatter = [[NSDateFormatter alloc] init];
}

- (void) updateTrackingData{
    [[PlistHelper new] saveData:trackingDict toPlist:TRACKING_INFO];
    [self loadTrackingData];
}

- (void) loadTrackingData
{
    trackingDict = [[PlistHelper new] getData:TRACKING_INFO];
}

-(NSString *) getCurrentTimeStamp
{
    NSDate *currentTimeStamp = [NSDate date];
    [formatter setDateFormat:FORMAT_DATE];
    NSString *formattedDate = [formatter stringFromDate: currentTimeStamp];
    [formatter setDateFormat:FORMAT_TIME_24];
    NSString *formattedTime = [formatter stringFromDate: currentTimeStamp];
    return [NSString stringWithFormat:@"%@T%@",formattedDate,formattedTime];
}

#pragma mark - Public Methods : Initialization of Data Store

- (void) initTrackingData
{
    trackingDict = [[PlistHelper new] getData:TRACKING_INFO];
    if (!trackingDict){
        trackingDict = [NSMutableDictionary dictionary];
    }
    [self updateTrackingData];
}

#pragma mark - Public Methods : Tracking

- (void) trackLookById : (NSString *) recordId
{
    NSMutableDictionary *looksDict;
    if([ReadData IsnulldictionaryFromDictionary:trackingDict forKey:KEY_TRACK_ELEMENT_LOOKS]){
        looksDict = [NSMutableDictionary dictionary];
    }
    else{
        looksDict = [[ReadData dictionaryFromDictionary:trackingDict forKey:KEY_TRACK_ELEMENT_LOOKS] mutableCopy];
    }
    
    NSMutableDictionary *item ;
    if([[looksDict allKeys] containsObject:recordId])
    {
        item = [[ReadData dictionaryFromDictionary:looksDict forKey:recordId] mutableCopy];
        NSInteger ctr = [ReadData integerValueFromDictionary:item forKey:KEY_ACCESSED_FREQUENCY] + 1;
        [item setObject: [NSNumber numberWithInteger:ctr] forKey:KEY_ACCESSED_FREQUENCY];
    }
    else
    {
        item = [NSMutableDictionary dictionary];
        [item setObject:recordId forKey:KEY_ID];
        [item setObject: [NSNumber numberWithInteger:1] forKey:KEY_ACCESSED_FREQUENCY];
    }
    [item setObject: [self getCurrentTimeStamp] forKey:KEY_ACCESSED_DTM];
    [looksDict setObject:item forKey:recordId];
    [trackingDict setObject:looksDict forKey:KEY_TRACK_ELEMENT_LOOKS];
    
    [self updateTrackingData];
}

#pragma mark - Fetch Information

- (NSArray *) fetchRecentlyViewedLooks{
    if([ReadData IsnulldictionaryFromDictionary:trackingDict forKey:KEY_TRACK_ELEMENT_LOOKS]) return [NSArray array];
    
    NSDictionary *looksDict = [ReadData dictionaryFromDictionary:trackingDict forKey:KEY_TRACK_ELEMENT_LOOKS];
    NSArray *looksArray = [looksDict allValues];
    
    NSSortDescriptor *sortDescriptor =[[NSSortDescriptor alloc] initWithKey:KEY_ACCESSED_DTM ascending:NO];
    NSSortDescriptor *titleDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_LOOK_HEADER ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, titleDescriptor, Nil];
    NSArray *sortedArray = [looksArray sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}

- (NSArray *) fetchPopularLooks{
    if([ReadData IsnulldictionaryFromDictionary:trackingDict forKey:KEY_TRACK_ELEMENT_LOOKS]) return [NSArray array];

    NSDictionary *looksDict = [ReadData dictionaryFromDictionary:trackingDict forKey:KEY_TRACK_ELEMENT_LOOKS];
    NSArray *looksArray = [looksDict allValues];
    
    NSSortDescriptor *sortDescriptor= [[NSSortDescriptor alloc] initWithKey:KEY_ACCESSED_FREQUENCY ascending:NO];
    NSSortDescriptor *titleDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_LOOK_HEADER ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, titleDescriptor, Nil];
    NSArray *sortedArray = [looksArray sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}


@end
