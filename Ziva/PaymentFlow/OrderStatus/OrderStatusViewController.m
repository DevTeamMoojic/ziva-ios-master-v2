//
//  OrderStatusViewController.m
//  Ziva
//
//  Created by Bharat on 14/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "OffersViewController.h"
#import "OrderStatusViewController.h"

@interface OrderStatusViewController ()
{
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIView *orderInfoV;
    
    __weak IBOutlet UIView *orderNumberV;
    __weak IBOutlet UILabel *orderNumberL;
    
    __weak IBOutlet UIView *orderItemsV;
    __weak IBOutlet UILabel *orderTypeL;
    __weak IBOutlet UILabel *orderItemsL;
    
    __weak IBOutlet UIView *orderDateV;
    __weak IBOutlet UILabel *orderDateL;
    
    __weak IBOutlet UIView *orderTimeV;
    __weak IBOutlet UILabel *orderTimeL;
    
    __weak IBOutlet UIView *orderModeOfPaymentV;
    __weak IBOutlet UILabel *orderModeOfPaymentL;
    
    __weak IBOutlet UIView *orderLocationV;
    __weak IBOutlet UILabel *orderLocationL;
    
    __weak IBOutlet UIView *offersCV;
    __weak OffersViewController *offersVC;
    
    __weak IBOutlet UIView *footerV;
    __weak IBOutlet UIButton *viewAppointmentB;
    __weak IBOutlet UIButton *tryAgainB;
    
    NSDateFormatter *formatter;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    
    NSInteger pageWidth;
    NSString *orderId ;
    
    AppDelegate *appD;
    MyCartController *cartC;
    
    CGFloat contentHeight;
    
}
@end

@implementation OrderStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    cartC = appD.sessionDelegate.mycartC;
    
    formatter = [[NSDateFormatter alloc] init];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - Layout

- (void) setupLayout
{
    pageWidth = CGRectGetWidth(self.view.bounds);
    
    CGSize cellcontentSz;
    cellcontentSz = [self getSizeForOfferCell];
    offersCV.frame = [UpdateFrame setSizeForView:offersCV usingHeight:cellcontentSz.height + 20 ];
    [offersVC setCellContentSize: cellcontentSz andPosition:BANNER_POSITION_BOTTOM];
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(topImageV.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
}

- (CGSize) getSizeForOfferCell
{
    CGFloat designWidth = 320;
    CGFloat designHeight = 160;
    
    CGFloat translatedWidth = CGRectGetWidth(self.view.bounds);
    CGFloat translatedHeight =(int)ceil(translatedWidth * designHeight/designWidth);
    
    return CGSizeMake(translatedWidth, translatedHeight);
}

- (NSString *) getLineItems : (NSArray *) itemArray
{
    NSMutableArray *lineItems = [NSMutableArray array];
    for(NSDictionary *item in itemArray){
        [lineItems addObject:[ReadData stringValueFromDictionary:item forKey:KEY_NAME]];
    }
    return [lineItems componentsJoinedByString:@","];
}

- (NSString *) getBookingDate : (NSString *) bookingSlot
{
    [formatter setDateFormat:FORMAT_DATE_TIME];
    NSDate *tmpDate = [formatter dateFromString:bookingSlot];
    [formatter setDateFormat:FORMAT_DATE_CONFIRMATION];
    return [formatter stringFromDate : tmpDate];
}

- (NSString *) getBookingTime : (NSString *) bookingSlot
{
    [formatter setDateFormat:FORMAT_DATE_TIME];
    NSDate *tmpDate = [formatter dateFromString:bookingSlot];
    [formatter setDateFormat:FORMAT_TIME_12];
    return [formatter stringFromDate : tmpDate];
}

-(NSString *) getOrderTypeDesc : (NSString *) orderType
{
    NSString *desc = @"";
    if([orderType isEqualToString:ORDER_TYPE_SERVICES]){
        desc = @"Services";
    }
    else if([orderType isEqualToString:ORDER_TYPE_PRODUCTS]){
        desc = @"Product";
    }
    else if([orderType isEqualToString:ORDER_TYPE_PACKAGES]){
        desc = @"Package";
    }
    return desc;
}

#pragma mark - Public Methods

- (void) showPaymentConfirmMsg{
    screenTitleL.text = @"CONFIRMATION";
    viewAppointmentB.hidden = NO;
    [viewAppointmentB setTitle:@"View in My Transaction" forState:UIControlStateNormal];
    tryAgainB.hidden = YES;
    [self displayOrderInfo];
}

- (void) showPaymentFailureMsg
{
    screenTitleL.text = @"PAYMENT FAILED";
    viewAppointmentB.hidden = YES;
    tryAgainB.hidden = NO;
    [tryAgainB setTitle:@"CONFIRMATION" forState:UIControlStateNormal];
    [self displayOrderInfo];
    footerV.hidden = NO;
}

#pragma mark - Methods

- (void) tabChanged
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
}

- (void) displayOrderInfo
{
    [offersVC reloadList];
    [self resetOffsetAllTabs];
    
    NSDictionary *dataDict = [cartC fetchOrderInfo];
    NSString *orderType = [ReadData stringValueFromDictionary:dataDict forKey:KEY_ORDER_TYPE];
    
    orderNumberL.text = [ReadData recordIdFromDictionary:dataDict forKey:KEY_ID];
    orderId = [ReadData recordIdFromDictionary:dataDict forKey:KEY_ID];
    orderTypeL.text = [self getOrderTypeDesc:orderType];
    orderItemsL.text = [self getLineItems : cartC.cartItems];
    NSString *bookDate = [[[ReadData stringValueFromDictionary:dataDict forKey:KEY_ORDER_BOOKDATE] stringByReplacingOccurrencesOfString:@"T" withString:@" "] substringWithRange:NSMakeRange(0, 19)];
    orderDateL.text =[self getBookingDate:bookDate];
    orderTimeL.text =[self getBookingTime:bookDate];
    orderModeOfPaymentL.text = cartC.modeOfPayment;
    orderLocationL.text = [ReadData stringValueFromDictionary:dataDict forKey:KEY_ORDER_LOCATION];
    
    CGFloat tmpHeight = [ResizeToFitContent getHeightForNLines:0 ForText:orderItemsL.text usingFontType:FONT_SEMIBOLD FontOfSize:12 andMaxWidth:CGRectGetWidth(orderItemsL.frame)];
    orderItemsL.frame = [UpdateFrame setSizeForView:orderItemsL usingHeight:tmpHeight];
    orderItemsV.frame = [UpdateFrame setSizeForView:orderItemsV usingHeight:tmpHeight + 20];
    
    tmpHeight = [ResizeToFitContent getHeightForNLines:0 ForText:orderLocationL.text usingFontType:FONT_SEMIBOLD FontOfSize:12 andMaxWidth:CGRectGetWidth(orderLocationL.frame)];
    orderLocationL.frame = [UpdateFrame setSizeForView:orderLocationL usingHeight:tmpHeight];
    orderLocationV.frame = [UpdateFrame setSizeForView:orderLocationV usingHeight:tmpHeight + 20];
    
    if(![orderType isEqualToString:ORDER_TYPE_SERVICES]){
        orderLocationV.frame = [UpdateFrame setSizeForView:orderLocationV usingHeight:0];
        orderTimeV.frame = [UpdateFrame setSizeForView:orderLocationV usingHeight:0];
        footerV.hidden = YES;
    }
    
    
    //Reposition Views
    orderDateV.frame = [UpdateFrame setPositionForView:orderDateV usingPositionY:CGRectGetMaxY(orderItemsV.frame)];
    orderTimeV.frame = [UpdateFrame setPositionForView:orderTimeV usingPositionY:CGRectGetMaxY(orderDateV.frame)];
    orderModeOfPaymentV.frame = [UpdateFrame setPositionForView:orderModeOfPaymentV usingPositionY:CGRectGetMaxY(orderTimeV.frame)];
    orderLocationV.frame = [UpdateFrame setPositionForView:orderLocationV usingPositionY:CGRectGetMaxY(orderModeOfPaymentV.frame)];
    orderInfoV.frame = [UpdateFrame setSizeForView:orderInfoV usingHeight:CGRectGetMaxY(orderLocationV.frame) + 15];
    offersCV.frame = [UpdateFrame setPositionForView:offersCV usingPositionY:CGRectGetMaxY(orderInfoV.frame)];
    footerV.frame= [UpdateFrame setPositionForView:footerV usingPositionY:CGRectGetMaxY(offersCV.frame)];
    
    contentHeight = CGRectGetMaxY(footerV.frame);
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self tabChanged];
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + contentHeight;
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContentOffset
{
    return [mainscrollView contentOffset];
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [self setScrollContentOffset : offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
                CGFloat alphaLevel = 1;
                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
                if (fabs(y) < BLUR_MAX_Y)
                {
                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
                }
                else
                {
                    alphaLevel = 0;
                }
                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset)  ];
        }
    }
}


#pragma mark - Events

- (IBAction)closeBPressed:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onCompleted_Booking)])
    {
        [self.delegate onCompleted_Booking] ;
    }
    
}

- (IBAction)viewAppointmentBPressed:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onView_Appointment:)])
    {
        [self.delegate onView_Appointment:orderId] ;
    }
}

- (IBAction)tryAgainBPressed:(UIButton *)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(onRetry_Booking)])
    {
        [self.delegate onRetry_Booking] ;
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embed_offers_bottom"])
    {
        offersVC = (OffersViewController *) segue.destinationViewController;
        
    }
    
}
@end
