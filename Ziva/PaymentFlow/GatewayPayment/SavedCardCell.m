//
//  SavedCardCell.m
//  Ziva
//
//  Created by Bharat on 22/10/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "SavedCardCell.h"

@implementation SavedCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
