//
//  MyProfileViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//
#import "DatePickerViewController.h"
#import "MyProfileViewController.h"

#define FORMAT_DATE_PROFILE @"dd-MM-yyyy"

@interface MyProfileViewController ()<MBProgressHUDDelegate, APIDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, DatePickerDelegate>
{
     MBProgressHUD *HUD;
    
    __weak IBOutlet UIImageView *blurredImgV;
    __weak IBOutlet UIView *transparencyV;
    
    __weak IBOutlet UIScrollView *mainscrollV;
    
    __weak IBOutlet UIView *profileV;
    __weak IBOutlet UIImageView *profileImgV;
    __weak IBOutlet UIImageView *placeholder_profileImgV;
    __weak IBOutlet UIActivityIndicatorView *loaderAIV;
    
    __weak IBOutlet UIView *fullNameV;
    __weak IBOutlet UIView *firstNameV;
    __weak IBOutlet UITextField *firstNameTxtF;
    __weak IBOutlet UIView *lastNameV;
    __weak IBOutlet UITextField *lastNameTxtF;
    __weak IBOutlet UIView *mobileV;
    __weak IBOutlet UITextField *mobileTxtF;
    
    __weak IBOutlet UIView *emailV;
    __weak IBOutlet UITextField *emailTxtF;

    __weak IBOutlet UIView *genderV;
    __weak IBOutlet UIButton *maleB;
    __weak IBOutlet UIButton *femaleB;

    __weak IBOutlet UIView *dateofbirthV;
    __weak IBOutlet UITextField *dateofbirthTxtF;

    __weak IBOutlet UIView *birthdayPickerCV;
    __weak DatePickerViewController *birthdayPickerVC;
    
    NSDate *birthDate;
    NSDateFormatter *dateFormatter;
    
    AppDelegate *appD;
}
@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    dateFormatter = [[NSDateFormatter alloc] init];
    [self setupData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void) setupData
{
    [self setupBlurEffect];
    
    profileV.layer.borderColor = [UIColor brandOrangeDisplayColor].CGColor;
    
    [birthdayPickerVC setMaximumDate:[NSDate date]];
    
    firstNameTxtF.text = [appD getLoggedInUserFirstName];
    lastNameTxtF.text = [appD getLoggedInUserLastName];
    mobileTxtF.text = [appD getLoggedInUserMobileNumber];
    emailTxtF.text = [appD getLoggedInUserEmailAddress];
    maleB.selected = [[appD getLoggedInUserGender] isEqualToString:GENDER_MALE];
    femaleB.selected = [[appD getLoggedInUserGender] isEqualToString:GENDER_FEMALE];
    
    NSString *profileBirthDate = [[appD getLoggedInUserBirthDate] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    if([appD isEmptyString:profileBirthDate]){
        dateofbirthTxtF.text = @"";
    }
    else{
        [dateFormatter setDateFormat:FORMAT_DATE_TIME];
        birthDate = [dateFormatter dateFromString:profileBirthDate];
        [dateFormatter setDateFormat:FORMAT_DATE_PROFILE];
        dateofbirthTxtF.text = [dateFormatter stringFromDate:birthDate];
    }
    
    [self updated_ProfilePhoto];
}

- (void) setupBlurEffect
{
    transparencyV.hidden = YES;
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = blurredImgV.bounds;
    [blurredImgV addSubview:visualEffectView];
}

- (void) displayDate
{
    birthDate = [birthdayPickerVC selectedDate];
    [dateFormatter setDateFormat:FORMAT_DATE_PROFILE];
    dateofbirthTxtF.text = [dateFormatter stringFromDate:birthDate];
}

-(void) processResponse : (NSDictionary *) resultsDict
{
    NSUserDefaults *standardDef = [NSUserDefaults standardUserDefaults];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_EMAIL] forKey:USER_EMAIL];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_LASTNAME] forKey:USER_LASTNAME];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_FIRSTNAME] forKey:USER_FIRSTNAME];
    [standardDef setValue: [ReadData genderIdFromDictionary:resultsDict forKey:KEY_USER_GENDER] forKey:USER_GENDER];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_IMAGE_URL] forKey:USER_PROFILE_URL];
    [standardDef setValue: [ReadData stringValueFromDictionary:resultsDict forKey:KEY_USER_DATEOFBIRTH] forKey:USER_BIRTHDATE];
    
    [standardDef synchronize];
    
    [appD profileUpdated];
    
    [self updated_ProfilePhoto];
}

- (void) updated_ProfilePhoto
{
    
    NSString *url = [appD getLoggedInUserProfileURL];
    // URL Check
    if (![url isEqualToString:@""])
    {
        NSURL *imageURL = [NSURL URLWithString:url];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:imageURL cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60 ];
        
        [profileImgV setImageWithURLRequest:urlRequest
                           placeholderImage:nil
                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             if(image)
             {
                 [profileImgV setImage:image];
                 [blurredImgV setImage:image];
                 [UIView animateWithDuration:IMAGE_VIEW_TOGGLE
                                       delay:0 options:UIViewAnimationOptionCurveLinear
                                  animations:^(void)
                  {
                      
                      [placeholder_profileImgV setAlpha:0 ];
                      [profileImgV setAlpha:1 ];
                  }
                                  completion:^(BOOL finished)
                  {
                  }];
             }
             
             
             
         }
                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             
         }];
    }
}

- (void) showErrorMessage : (int)errorCode
{
    NSString *message = @"";
    switch (errorCode) {
        case NETWORKERRORCODE: {
            message = NETWORKERROR;
        }
            break;
        case FIRSTNAMEEMPTY: {
            message = @"First Name cannot be blank.";
        }
            break;
        case LASTNAMEEMPTY: {
            message = @"Last Name cannot be blank.";
        }
            break;
        case EMAILEMPTY: {
            message = @"Email cannot be blank.";
        }
            break;
        case EMAILWRONG: {
            message = @"Enter valid email address.";
        }
            break;
        case DOBEMPTY: {
            message = @"Date of birth cannot be blank.";
        }
            break;
        case GENDEREMPTY: {
            message = @"Gender cannot be blank.";
        }
            break;
    }
    
    UIAlertView *errorNotice = [[UIAlertView alloc] initWithTitle:ALERT_TITLE_ERROR message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorNotice show];
}

- (BOOL) isValid
{
    [UIResponder dismissKeyboard];
    [mainscrollV setContentOffset:CGPointZero animated:YES];

    if ([appD isEmptyString:firstNameTxtF.text])
    {
        [self showErrorMessage:FIRSTNAMEEMPTY];
        return  NO;
    }
    if ([appD isEmptyString:lastNameTxtF.text])
    {
        [self showErrorMessage:LASTNAMEEMPTY];
        return  NO;
    }
    if ([appD isEmptyString:emailTxtF.text])
    {
        [self showErrorMessage:EMAILEMPTY];
        return  NO;
    }
    if(![appD NSStringIsValidEmail:emailTxtF.text])
    {
        [self showErrorMessage:EMAILWRONG];
        return  NO;
    }
    if(!maleB.selected && !femaleB.selected){
        [self showErrorMessage:GENDEREMPTY];
        return  NO;
    }
    return YES;
}

- (void) updateProfile
{
    if ([InternetCheck isOnline]){
        
        [self hudShowWithMessage:@"Loading"];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:[appD getLoggedInUserId] forKey:KEY_ID];
        [paramDict setObject:firstNameTxtF.text forKey:KEY_USER_FIRSTNAME];
        [paramDict setObject:lastNameTxtF.text forKey:KEY_USER_LASTNAME];
        [paramDict setObject:emailTxtF.text forKey:KEY_USER_EMAIL];
        [paramDict setObject:(maleB.selected? GENDER_MALE : GENDER_FEMALE) forKey:KEY_USER_GENDER];
        if(birthDate){
            [dateFormatter setDateFormat:FORMAT_DATE];
            [paramDict setObject:[dateFormatter stringFromDate:birthDate] forKey:KEY_USER_DATEOFBIRTH];
        }
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_UPDATE_PROFILE Param:paramDict];
    }
    
}

- (void) updateProfilePhoto
{
    if ([InternetCheck isOnline]){
        
        [loaderAIV startAnimating];
        
        NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
        [paramDict setObject:[appD getLoggedInUserId] forKey:KEY_ID];
        [paramDict setObject:[ImageHelper imageToNSString:profileImgV.image] forKey:@"ImageString"];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_UPDATE_PROFILE_PICTURE Param:paramDict];
    }
    
}

#pragma mark - Generic Show/hide views

- (void) showPickerV : (UIView *) view
{
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         view.frame = CGRectOffset(view.frame,0,-1* CGRectGetHeight( view.frame));
         
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
     }];
}

- (void) hidePickerV : (UIView *) view
{
    // 1 - Dismiss keyboard
    [UIResponder dismissKeyboard];
    
    // 2 - Disable User Interaction
    [self.view setUserInteractionEnabled:NO];
    
    // 3 - Animate
    [UIView animateWithDuration:TIME_VIEW_TOGGLE
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         view.frame = CGRectOffset(view.frame,0, CGRectGetHeight( view.frame));
         
     }
                     completion:^(BOOL finished)
     {
         // Enable User Interaction
         [self.view setUserInteractionEnabled:YES];
         //[self showNextResponder:view];
     }];
    
}


#pragma mark - Date Picker Delegate

- (void) dateSelected ;
{
    [self displayDate];
    [self hideDatePicker];
    
}
- (void) hideDatePicker
{
    [self hidePickerV:birthdayPickerCV];
}


#pragma mark - HUD & Delegate methods

- (void)hudShowWithMessage:(NSString *)message
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.dimBackground = YES;
    HUD.labelText = message;
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
    
    [HUD show:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [HUD removeFromSuperview];
    HUD = nil;
}

#pragma mark - ImagePicker delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:Nil];
    profileImgV.image = [ImageHelper resizeImage: [info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    [blurredImgV setImage:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    profileImgV.alpha = 1;
    [self updateProfilePhoto];
}

#pragma mark - Text Responder

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if([textField isEqual:firstNameTxtF]){
        [lastNameTxtF becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:mobileTxtF] || [textField isEqual:dateofbirthTxtF]){
        return NO;
    }
    else{
        return YES;
    }
}


#pragma mark - Events

- (IBAction)backBPressed:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)profilepicBPressed: (UIButton *)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate=self;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)birthdateBPressed: (UIButton *)sender
{
    [UIResponder dismissKeyboard];
    if(birthDate){
        [birthdayPickerVC setDate:birthDate];
    }
    
    [self showPickerV:birthdayPickerCV];
    
}

- (IBAction)genderBPressed: (UIButton *)sender
{
    if(sender.selected) return;
    if([sender isEqual:maleB]){
        maleB.selected = YES;
        femaleB.selected = NO;
    }
    if([sender isEqual:femaleB]){
        maleB.selected = NO;
        femaleB.selected = YES;
    }
}


- (IBAction)saveBPressed:(UIButton *)sender{
    [UIResponder dismissKeyboard];
    if([self isValid]){
        [self updateProfile];
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embed_profile_birthday"])
    {
        birthdayPickerVC = (DatePickerViewController *)segue.destinationViewController;
        birthdayPickerVC.delegate =self;
    }
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    [HUD hide:YES];
    [loaderAIV stopAnimating];
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [HUD hide:YES];
        }
        return;
    }
    
    if([apiName isEqualToString:API_UPDATE_PROFILE_PICTURE]){
        [self processResponse:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
        
    }
    if([apiName isEqualToString:API_UPDATE_PROFILE]){
        [self processResponse:[ReadData dictionaryFromDictionary:response forKey:RESPONSEKEY_DATA]];
    }
}

@end
