//
//  LeftMenuViewController.h
//  Ziva
//
//  Created by Bharat on 27/02/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface LeftMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>
{
    NSArray *menuTitles;
    NSArray *menuImages;
}

- (void) profileUpdated ;

- (void) locationUpdated ;

- (void) backToHome ;

@end
