//
//  RootViewController.h
//  Ziva
//
//  Created by  Bharat on 01/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface RootViewController : RESideMenu <RESideMenuDelegate>

- (void) profileUpdated ;

- (void) locationUpdated ;

- (void) backToHome ;

@end
