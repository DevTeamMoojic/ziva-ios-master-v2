//
//  PackageCategoryListViewController.m
//  Ziva
//
//  Created by Bharat on 24/09/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "PackageCell.h"
#import "PackagesViewController.h"
#import "PackageCategoryListViewController.h"

@interface PackageCategoryListViewController ()<UICollectionViewDelegateFlowLayout>
{
    NSArray *list;
    CGFloat contentHeight;
    
    CGSize layoutsize;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    __weak IBOutlet UICollectionView *collectionlistV;
    __weak IBOutlet UIPageControl *pagerControl;
    
    AppDelegate *appD;
    
    NSInteger pageIndex;
    
}
@end

@implementation PackageCategoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    [self resetLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

-(void) resetLayout
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
}

#pragma mark - Public Methods

- (void) loadData: (NSArray *) listArray  withCellContentSize : (CGSize) contentsize;{
    list = listArray;
    layoutsize = contentsize;
    
    collectionlistV.frame= [UpdateFrame setSizeForView:collectionlistV usingSize:layoutsize];
    
    contentHeight = CGRectGetMaxY(pagerControl.frame) + 3;
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    [self setupLayout];
}

- (void) setupLayout
{
    pageIndex = 0;
    pagerControl.currentPage = pageIndex;
    pagerControl.numberOfPages = [list count];
    pagerControl.hidden = ([list count] < 2);
    
    [collectionlistV reloadData];
    [collectionlistV setContentOffset:CGPointZero animated:NO];
}


#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [list count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return layoutsize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // set cell
    PackageCell *cell = (PackageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"packagecell" forIndexPath:indexPath];
    
    {
        cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize];
        cell.imageCV.frame = [UpdateFrame setSizeForView:cell.mainV usingHeight: (CGRectGetHeight(cell.mainV.frame) - CGRectGetHeight(cell.infoCV.frame))];
    }
    
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell:item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *item = list[indexPath.row];
    [self performSegueWithIdentifier:@"showpackagedetail" sender:item];
}


#pragma mark - Scroll

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSUInteger page = floor((scrollView.contentOffset.x - layoutsize.width / 2) / layoutsize.width) + 1;
    if (pageIndex != page)
    {
        pageIndex = page ;
        pagerControl.currentPage = pageIndex;
    }
}

@end
