//
//  HomeViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "LandingPageViewController.h"
#import "ChooseServicesViewController.h"
#import "StylistsViewController.h"
#import "HomeViewController.h"
#import "RESideMenu.h"

#define TABS_COUNT 4

@interface HomeViewController ()
{
    __weak IBOutlet UIView *optionsV;    
    __weak IBOutlet UILabel *screenTitleL;    
    
    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UIView *topImageV;
    __weak IBOutlet UIImageView *mainImgV;
    
    __weak IBOutlet UIScrollView *scrollviewOnTop;
    __weak IBOutlet UIScrollView *tabscrollView;
    
    //Landing Tab
    __weak IBOutlet UIView *optionCV_0;
    __weak IBOutlet UIButton *optionIconB_0;
    __weak IBOutlet UILabel *optionHeadingL_0;
    
    //Home Services Tab
    __weak IBOutlet UIView *optionCV_1;
    __weak IBOutlet UIButton *optionIconB_1;
    __weak IBOutlet UILabel *optionHeadingL_1;

    //Salon Services Tab
    __weak IBOutlet UIView *optionCV_2;
    __weak IBOutlet UIButton *optionIconB_2;
    __weak IBOutlet UILabel *optionHeadingL_2;
    
    //Stylists Tab
    __weak IBOutlet UIView *optionCV_3;
    __weak IBOutlet UIButton *optionIconB_3;
    __weak IBOutlet UILabel *optionHeadingL_3;
    
    __weak IBOutlet UIScrollView *tabcontentscrollView;
    
    __weak LandingPageViewController *landingpageVC;
   // __weak ChooseServicesViewController *homeservicesVC;
   // __weak ChooseServicesViewController *salonservicesVC;
    __weak StylistsViewController *stylistsVC;
    
    CGRect cachedImageViewSize;
    CGFloat yscrollOffset;
    
    BOOL isBackFromOtherView;
    BOOL isContentSizeUpdated;
    BOOL isUpdating;
    
    NSUInteger pageIndex;
    NSInteger pageWidth;
    NSInteger pageHeight;
    
    AppDelegate *appD;    
}
@end

@implementation HomeViewController
@synthesize homeservicesVC = _homeservicesVC;
@synthesize salonservicesVC = _salonservicesVC;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appD = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appD.sessionDelegate.mycartC getCoupons];
    [appD startLocationServices];
    
    [self.view addGestureRecognizer:scrollviewOnTop.panGestureRecognizer];
    
    // commit the code for time beaing.
   /*
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabscrollView.frame = [UpdateFrame setPositionForView:tabscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(tabscrollView.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(tabscrollView.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    [self loadcontentScrollView];*/
    
    // set init for choose view controllers.
    _homeservicesVC = [[ChooseServicesViewController alloc]init];
    _salonservicesVC = [[ChooseServicesViewController alloc]init];
    
    [self loadcontentScrollView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self resetLayout];
    // new code minnarao
    //Position elements
    {
        topImageV.frame = [UpdateFrame setSizeForView:topImageV usingHeight:[ResizeToFitContent getHeightForParallaxImage:self.view.frame]];
        
        yscrollOffset = CGRectGetHeight(topImageV.frame) - IMAGE_SCROLL_OFFSET_Y - CGRectGetHeight(optionsV.frame);
        mainImgV.frame = topImageV.frame ;
        
        containerView.frame = [UpdateFrame setSizeForView:containerView usingHeight:(CGRectGetHeight(self.view.bounds) + yscrollOffset)];
        
        tabscrollView.frame = [UpdateFrame setPositionForView:tabscrollView usingPositionY: CGRectGetMaxY(topImageV.frame)];
        
        tabcontentscrollView.frame= [UpdateFrame setPositionForView:tabcontentscrollView usingPositionY: CGRectGetMaxY(tabscrollView.frame)];
        tabcontentscrollView.frame = [UpdateFrame setSizeForView:tabcontentscrollView usingHeight: CGRectGetHeight(containerView.frame) - CGRectGetMaxY(tabscrollView.frame)];
    }
    
    
    CGPoint scrollviewOrigin = scrollviewOnTop.frame.origin;
    scrollviewOnTop.scrollIndicatorInsets = UIEdgeInsetsMake(-scrollviewOrigin.y, 0, scrollviewOrigin.y, scrollviewOrigin.x);
    
    
    if (!isBackFromOtherView)
        cachedImageViewSize = mainImgV.frame;
    else
        isBackFromOtherView = NO;
}

#pragma mark - Public Methods

- (void) launchDetailView{
    isBackFromOtherView = YES;
}

- (void) resetLayout
{
    isUpdating = YES;
    pageIndex = 0;
    [self tabChanged:pageIndex];
    [tabcontentscrollView setContentOffset:CGPointMake(pageIndex*pageWidth, 0) animated:YES];
    [self resetOffsetAllTabs];
}

#pragma mark - Setup Tab Containers

- (CGFloat) calculateImageHeight
{
    CGFloat designScreenHeight = 667;
    CGFloat designHeight = 230;
    
    CGFloat translatedHeight = ceil(designHeight * CGRectGetHeight(self.view.frame)/designScreenHeight);
    
    return (int)translatedHeight;
}

- (NSString *) getStoryboardIdentifer : (NSInteger) ctr
{
    NSString *retVal = @"";
    switch (ctr) {
        case 0:
            retVal = @"landingpageVC";
            break;
        case 1:
        case 2:
            retVal = @"chooseservicesVC";
            break;
        case 3:
            retVal = @"stylistsVC";
            break;
            
        default:
            break;
    }
    return retVal;
}

- (void) loadcontentScrollView
{
    //Submenus tab content scroll view
    pageWidth = CGRectGetWidth(tabcontentscrollView.frame);
    tabcontentscrollView.contentSize = CGSizeMake(pageWidth * TABS_COUNT , CGRectGetHeight(tabcontentscrollView.frame));
    
    UIViewController *controller;
    for (NSInteger ctr = 0; ctr < TABS_COUNT; ctr++)
    {
 
        controller = [self.storyboard instantiateViewControllerWithIdentifier:[self getStoryboardIdentifer:ctr]];
        
        // new code minnarao.
        // add the controller's view to the scroll view
        if (ctr != 1 || ctr != 2){
        if (controller.view.superview == nil)
        {
            controller.view.frame = [UpdateFrame setPositionForView:tabcontentscrollView usingPositionX:pageWidth * ctr andPositionY:0];
            
            [self addChildViewController:controller];
            [tabcontentscrollView addSubview:controller.view];
            [controller didMoveToParentViewController:self];
        }
        }
        //Controller specific data load
        switch (ctr) {
            case 0:
                landingpageVC = (LandingPageViewController *) controller;
                [landingpageVC updateLayout];
                break;
            case 1:
                _salonservicesVC = (ChooseServicesViewController *) controller;
                [_salonservicesVC loadStoresForSalonServices];
                break;
            case 2:
               _homeservicesVC = (ChooseServicesViewController *) controller;
               [_homeservicesVC loadStoresForHomeServices];
                break;
            case 3:
                stylistsVC = (StylistsViewController *) controller;
                [stylistsVC updateLayout];
                break;
            default:
                break;
        }
    }
    pageIndex = 0;
    [self tabChanged:pageIndex];
}

- (void) tabChanged:(NSInteger)index
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize : index];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        CGPoint offset = [self getScrollContentOffset:index];
        offset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:offset animated:NO];
    }
    isContentSizeUpdated = NO;
    
    [self setSelectedOption:index];
}

-(void)setSelectedOption:(NSInteger)index
{
    for (int i = 0; i < TABS_COUNT; i++)
    {
        UIButton *btn = (UIButton *)[tabscrollView viewWithTag:i + 100];
        if (i != index  )
        {
            if (btn.selected) btn.selected = NO;
        }
        else{
            btn.selected = YES;
        }
        [self updateOptionHeading:i];
    }

}

-(void) updateOptionHeading : (NSInteger) index
{
    UIButton *btn = (UIButton *)[tabscrollView viewWithTag:index + 100];
    UIButton *iconB;
    UILabel *headingL;
    
    switch (index) {
        case 0:
            iconB = optionIconB_0;
            headingL = optionHeadingL_0;
            break;
        case 1:
            iconB = optionIconB_1;
            headingL = optionHeadingL_1;
            break;
        case 2:
            iconB = optionIconB_2;
            headingL = optionHeadingL_2;
            break;
        case 3:
            iconB = optionIconB_3;
            headingL = optionHeadingL_3;
            break;
        default:
            break;
    }
    iconB.selected = btn.selected;
    headingL.textColor = (btn.selected)? [UIColor colorWithRed:247.0/255 green:184.0/255 blue:42.0/255 alpha:1] : [UIColor whiteColor];    
}

#pragma mark - Expand on Scroll animation

- (CGSize) setContentSize :(NSInteger)index
{
    CGFloat height = CGRectGetHeight(mainImgV.frame) + CGRectGetHeight(tabscrollView.frame);
    switch (index) {
        case 0:
            height+=[landingpageVC getContentHeight];
            break;
        case 1:
            height+=[_salonservicesVC getContentHeight];
            break;
        case 2:
            height+=[_homeservicesVC getContentHeight];
            break;
        case 3:
            height+=[stylistsVC getContentHeight];
            break;
        default:
            break;
    }
    return CGSizeMake(pageWidth, height);
}

- (void) setScrollContentOffset: (CGPoint) offset forPage:(NSInteger)index
{
    switch (index) {
        case 0:
            [landingpageVC scrollContent:offset];
            break;
        case 1:
            [_salonservicesVC scrollContent:offset];
            break;
        case 2:
            [_homeservicesVC scrollContent:offset];
            break;
        case 3:
            [stylistsVC scrollContent:offset];
            break;
        default:
            break;
    }
}

- (CGPoint) getScrollContentOffset :(NSInteger)index
{
    CGPoint offset;
    
    switch (index) {
        case 0:
            offset = [landingpageVC getScrollContent];
            break;
        case 1:
            offset = [_salonservicesVC getScrollContent];
            break;
        case 2:
            offset = [_homeservicesVC getScrollContent];
            break;
        case 3:
            offset = [stylistsVC getScrollContent];
            break;
            
        default:
            break;
    }
    return offset;
}

- (void) resetOffsetAllTabs
{
    CGPoint offset = CGPointZero;
    [landingpageVC scrollContent:offset];
    [_homeservicesVC scrollContent:offset];
    [_salonservicesVC scrollContent:offset];
    [stylistsVC scrollContent:offset];
}

-(void) updateContentSize : (CGPoint) contentOffset;
{
    CGPoint oldOffset = scrollviewOnTop.contentOffset;
    isContentSizeUpdated = YES;
    CGSize newsize =  [self setContentSize : pageIndex];
    [scrollviewOnTop setContentOffset:CGPointZero animated:NO];
    scrollviewOnTop.contentSize = newsize;
    if (oldOffset.y >= yscrollOffset)
    {
        contentOffset.y += yscrollOffset;
        [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    }
    [scrollviewOnTop setContentOffset:contentOffset animated:NO];
    
    isContentSizeUpdated = NO;
}

#pragma mark - Scroll Events

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == scrollviewOnTop)
    {
        {
            if (isContentSizeUpdated)  return;
            
            CGRect scrolledBoundsForContainerView = containerView.bounds;
            if (scrollView.contentOffset.y <= yscrollOffset)
            {
                
                scrolledBoundsForContainerView.origin.y = scrollView.contentOffset.y ;
                containerView.bounds = scrolledBoundsForContainerView;
                
                //Reset offset for all tabs
                if (scrollView.contentOffset.y <= 0) [self resetOffsetAllTabs];
                
                
                CGFloat y = -scrollView.contentOffset.y;
//                CGFloat alphaLevel = 1;
//                CGFloat BLUR_MAX_Y = IMAGE_SCROLL_OFFSET_Y + CGRectGetHeight(topImageV.frame) - CGRectGetMinY(optionsV.frame);
//                if (fabs(y) < BLUR_MAX_Y)
//                {
//                    alphaLevel = (BLUR_MAX_Y - fabs(y))/(BLUR_MAX_Y);
//                }
//                else
//                {
//                    alphaLevel = 0;
//                }
//                
                //[screenTitleL setAlpha:alphaLevel];
                if (y > 0)
                {
                    mainImgV.frame = CGRectInset(cachedImageViewSize, 0, -y/2);
                    mainImgV.center = CGPointMake(mainImgV.center.x, mainImgV.center.y + y/2);
                }
                else
                {
                    mainImgV.frame = [UpdateFrame setPositionForView:mainImgV usingPositionY:y];
                }
                return;
            }
            
            scrolledBoundsForContainerView.origin.y = yscrollOffset ;
            containerView.bounds = scrolledBoundsForContainerView;
            
            [self setScrollContentOffset:CGPointMake(0, scrollView.contentOffset.y - yscrollOffset) forPage:pageIndex ];
        }
    }
    else if(scrollView == tabcontentscrollView)
    {
        if (isUpdating) return;
        NSUInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (pageIndex != page)
        {
            pageIndex = page;
            [self tabChanged:pageIndex];
    
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if(scrollView == tabcontentscrollView)
    {
        if (isUpdating) isUpdating = NO;
    }
}

#pragma mark - Events

- (IBAction)showMenuBPressed: (UIButton *)sender
{
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (IBAction)searchBPressed: (UIButton *)sender
{
    
}

- (IBAction)chatBPressed: (UIButton *)sender
{
    [appD startChatSupport:@""];
}

- (IBAction)optionBPressed: (UIButton *)sender
{
    NSUInteger page = sender.tag - 100;
    
    // Minnarao new code.
    if (page == 1)
    {
        //[salonservicesVC loadStoresForSalonServices];

        [self.navigationController pushViewController:_salonservicesVC animated:YES];
    }
    else if (page == 2)
    {
        [self.navigationController pushViewController:_homeservicesVC animated:YES];
    }

    else if (pageIndex != page)
    {
        isUpdating = YES;
        pageIndex = page;
        [tabcontentscrollView setContentOffset:CGPointMake(pageIndex*pageWidth, 0) animated:YES];
        [self tabChanged:pageIndex];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

@end
