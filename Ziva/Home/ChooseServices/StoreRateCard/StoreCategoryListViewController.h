//
//  StoreCategoryListViewController.h
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreCategoryListViewController : UIViewController


- (void) loadData: (NSArray *) listArray andIsRateCard : (BOOL) isRateCard;
- (void) reloadData ;

- (CGFloat) getContentHeight;
- (void) scrollContent : (CGPoint) offset;
- (CGPoint) getScrollContent ;

@end
