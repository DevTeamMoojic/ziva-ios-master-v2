//
//  StoreCategoryListViewController.m
//  Ziva
//
//  Created by Bharat on 05/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ServicesCell.h"
#import "StoreRateCardViewController.h"
#import "StoreCategoryListViewController.h"

@interface StoreCategoryListViewController (){
    CGFloat contentHeight;
    NSArray *list;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UILabel *noresultsL;
    
    BOOL isStoresRateCard;
    AppDelegate *appD;
}
@end

@implementation StoreCategoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [mainscrollView removeGestureRecognizer:mainscrollView.panGestureRecognizer];
    [self resetLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public Methods

- (void) loadData: (NSArray *) listArray andIsRateCard : (BOOL) isRateCard{
    
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_SERVICE_TITLE ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    list = [listArray sortedArrayUsingDescriptors:sortDescriptors];
    
    isStoresRateCard = isRateCard;
    [listTblV reloadData];
    
    listTblV.frame = [UpdateFrame setSizeForView:listTblV usingHeight:[self calculateHeightForTable]];
    
    contentHeight = CGRectGetMaxY(listTblV.frame);
    
    if (contentHeight <= CGRectGetHeight(mainscrollView.bounds))
    {
        contentHeight = CGRectGetHeight(mainscrollView.bounds);
        mainscrollView.scrollEnabled = NO;
    }
    
    noresultsL.hidden = ([list count] > 0);
    listTblV.hidden = ([list count] == 0);
 
    CGPoint scrollToPoint = CGPointZero;
    StoreRateCardViewController *parentVC = (StoreRateCardViewController *)self.parentViewController;
    [mainscrollView setContentOffset:scrollToPoint animated:YES];
    [parentVC updateContentSize : scrollToPoint];
}

- (void) reloadData
{
    [listTblV reloadData];
}

#pragma mark - Methods

-(void) resetLayout
{
    mainscrollView.contentOffset = CGPointZero;
    contentHeight = CGRectGetHeight(self.view.bounds);
}

- (CGFloat) calculateHeightForTable{
    CGFloat height = 0;
    for(NSInteger ctr =0; ctr < [list count]; ctr++){
        height += [self calculateheightForRow:ctr];
    }
    height +=80;
    return height;
}

#pragma mark - Scroll content

- (CGFloat) getContentHeight;
{
    return contentHeight;
}

- (void) scrollContent : (CGPoint) offset
{
    mainscrollView.contentOffset = offset;
}

- (CGPoint) getScrollContent
{
    return mainscrollView.contentOffset;
}

#pragma mark - Table view data source

-(CGFloat) calculateheightForRow:(NSInteger)row{
    CGFloat height = 60.0f;
    //NSDictionary *item = list[indexPath.row];
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [list count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self calculateheightForRow:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ServicesCell *cell = (ServicesCell *)[tableView dequeueReusableCellWithIdentifier:@"servicescell" forIndexPath:indexPath];
    NSDictionary *item = list[indexPath.row];
    cell.isStoreRateCard = isStoresRateCard;
    [cell configureDataForCell :item];
    
    cell.showOffersB.tag = cell.selectServicesB.tag = cell.actionB.tag = indexPath.row;
    cell.selectServicesB.selected = [appD.sessionDelegate.mycartC isAddedToCart:[ReadData recordIdFromDictionary:item forKey:KEY_ID]];
    
    if(!cell.showOffersB.hidden){
        [cell.actionB addTarget:self action:@selector(expandSectionBPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (!cell.selectServicesB.hidden){
        [cell.actionB addTarget:self action:@selector(addToCartBPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

-(void)expandSectionBPressed:(id)sender
{
//    isExpandingCollapsing = YES;
//    
//    UIButton *btn = (UIButton *)sender;
//    NSInteger selectedRow = btn.tag;
//    NSMutableDictionary *item = lstReservations[selectedRow];
//    
//    btn.selected = !btn.selected;
//    [item setObject: btn.selected ? @"1" : @"0" forKey:KEY_SECTION_DISPLAYSTATE];
//    
//    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:selectedRow inSection:0];
//    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
//    [listingTblV reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

-(void)addToCartBPressed:(UIButton *)sender{
    NSInteger selectedRow = sender.tag;
    NSDictionary *item = list[selectedRow];
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:selectedRow inSection:0];
    ServicesCell *cell = (ServicesCell *)[listTblV cellForRowAtIndexPath:rowToReload];
    cell.selectServicesB.selected = !cell.selectServicesB.selected;
    
    StoreRateCardViewController *parentVC = (StoreRateCardViewController *)self.parentViewController;
    if(cell.selectServicesB.selected){
        [parentVC addItemToCart:item];
    }
    else{
        [parentVC removeItemFromCart:item];
    }
}


@end
