//
//  ServicesListingViewController.m
//  Ziva
//
//  Created by Bharat on 15/11/16.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "ServiceCell.h"
#import "KeywordCell.h"
#import "ServicesListingViewController.h"
#import "ChooseServicesViewController.h"

@interface ServicesListingViewController ()
{
    NSArray *lstKeywords;
    NSArray *lstServices;
    
    __weak IBOutlet UITableView *listTblV;
    __weak IBOutlet UITableView *listServicesTblV;
    
    __weak IBOutlet UILabel *noresultsL;
    
    BOOL isKeywordList;
}
@end

@implementation ServicesListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void) loadKeywords : (NSArray *) filteredDataList
{
    isKeywordList = YES;
    listTblV.hidden = NO;
    listServicesTblV.hidden = !listTblV.hidden;
    [listTblV setContentOffset:CGPointZero animated:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:KEY_NAME ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    lstKeywords = [filteredDataList sortedArrayUsingDescriptors:sortDescriptors];
    [listTblV reloadData];
    
    noresultsL.hidden = ([lstKeywords count] > 0);
    listTblV.hidden = ([lstKeywords count] == 0);
}

- (void) loadServices : (NSArray *) filteredDataList
{
    isKeywordList = NO;
    listTblV.hidden = YES;
    listServicesTblV.hidden = !listTblV.hidden;
    [listServicesTblV setContentOffset:CGPointZero animated:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"ServiceName" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    lstServices = [filteredDataList sortedArrayUsingDescriptors:sortDescriptors];
    [listServicesTblV reloadData];
}

- (CGFloat) getContentHeight
{
    CGFloat height = 0;
    for(NSInteger ctr = 0; ctr < [lstKeywords count]; ctr++){
        height += 40;
    }
    return height;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([tableView isEqual:listTblV]){
        return [lstKeywords count];
    }
    else{
        return [lstServices count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:listTblV]){
        return 60;
    }
    else{
        return 85;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([tableView isEqual:listTblV])
    {
        KeywordCell *cell = (KeywordCell *)[tableView dequeueReusableCellWithIdentifier:@"keywordcell" forIndexPath:indexPath];
        NSDictionary *item = lstKeywords[indexPath.row];
        [cell configureDataForCell :item];
        return cell;
    }
    else{
        ServiceCell *cell = (ServiceCell *)[tableView dequeueReusableCellWithIdentifier:@"servicecell" forIndexPath:indexPath];
        NSDictionary *item = lstServices[indexPath.row];
        [cell configureDataForCell :item];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if([tableView isEqual:listTblV]){
        NSDictionary *item = lstKeywords[indexPath.row];
        ChooseServicesViewController *parentVC = (ChooseServicesViewController *)self.parentViewController;
        [parentVC getServicesByKeyword:[ReadData stringValueFromDictionary:item forKey:KEY_NAME]];
    }
    else{
        NSDictionary *item = lstServices[indexPath.row];
        ChooseServicesViewController *parentVC = (ChooseServicesViewController *)self.parentViewController;
        [parentVC displayStoreRateCardUsingService:item];
    }
}

@end
