//
//  StoreCell.h
//  Ziva
//
//  Created by Bharat on 03/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *imageCV;
@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;
@property (weak, nonatomic) IBOutlet UIImageView *placeholderImgV;

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *subtitleL;

@property (weak, nonatomic) IBOutlet UILabel *ratingL;

@property (weak, nonatomic) IBOutlet UIView *distanceV;
@property (weak, nonatomic) IBOutlet UILabel *distanceL;


- (void) configureDataForCell : (NSDictionary *) item;

@end
