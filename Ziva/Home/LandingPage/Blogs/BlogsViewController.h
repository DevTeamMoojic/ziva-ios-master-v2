//
//  BlogsViewController.h
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogsViewController : UIViewController

- (void) setCellContentSize : (CGSize) contentsize;

- (void) reloadList;

@end
