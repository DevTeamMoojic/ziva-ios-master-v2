//
//  BlogCell.h
//  Ziva
//
//  Created by Bharat on 03/07/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *mainV;

@property (weak, nonatomic) IBOutlet UIView *imageCV;
@property (weak, nonatomic) IBOutlet UIImageView *mainImgV;

@property (weak, nonatomic) IBOutlet UIView *titleCV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;

- (void) configureDataForCell : (NSDictionary *) item;

@end
