//
//  OffersViewController.m
//  Ziva
//
//  Created by Bharat on 30/06/2016.
//  Copyright © 2016 Ziva Lifestyle India Private Limited. All rights reserved.
//

#import "OfferCell.h"
#import "OffersViewController.h"

@interface OffersViewController ()<APIDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *list;
    CGSize layoutsize;
    
    __weak IBOutlet UILabel *noOffersL;
    __weak IBOutlet UICollectionView *collectionlistV;
    __weak IBOutlet UIPageControl *pagerControl;
    
    __weak IBOutlet UIActivityIndicatorView *sectionloaderAIV;
    
    AppDelegate *appD;
    
    NSInteger pageIndex;

    NSInteger banner_position;
    NSString *cellIdentifier;
}
@end

@implementation OffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    list = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Public Methods

- (void) setCellContentSize : (CGSize) contentsize andPosition : (NSInteger) position
{
    layoutsize = contentsize;
    banner_position = position;
    switch(banner_position){
       case BANNER_POSITION_TOP:
            cellIdentifier = @"offercell_top";
            break;
        case BANNER_POSITION_MIDDLE:
            cellIdentifier = @"offercell_middle";
            break;
        case BANNER_POSITION_BOTTOM:
            cellIdentifier = @"offercell_bottom";
            break;
    }
    
    if(banner_position == BANNER_POSITION_BOTTOM ){
        collectionlistV.frame= [UpdateFrame setSizeForView:collectionlistV usingHeight:layoutsize.height + 20];
    }
    else{
        collectionlistV.frame= [UpdateFrame setSizeForView:collectionlistV usingSize:layoutsize];
    }
}

- (void) reloadList
{
    if ([InternetCheck isOnline]){
        
        [sectionloaderAIV startAnimating];
        
        [[[APIHelper alloc] initWithDelegate:self] callAPI:API_GET_OFFERS Param:NULL];
    }
}

#pragma mark - Methods

- (void) processResponse : (NSDictionary *) dict
{
    NSArray *tmpArray = [ReadData arrayFromDictionary:dict forKey:RESPONSEKEY_DATA];
    if([list count] > 0 ) [list removeAllObjects];
    for(NSDictionary *item in tmpArray){
        
        if([ReadData integerValueFromDictionary:item forKey:KEY_POSITION] == banner_position)
        {
            [list addObject:item];
        }
    }
    if(([list count] == 0) && (banner_position != BANNER_POSITION_TOP)){
        for(NSDictionary *item in tmpArray){
            if([ReadData integerValueFromDictionary:item forKey:KEY_POSITION] == BANNER_POSITION_TOP)
            {
                [list addObject:item];
            }
        }
    }
    
    
    [self setupLayout];
}

- (void) setupLayout
{
    pageIndex = 0;
    pagerControl.currentPage = pageIndex;
    pagerControl.numberOfPages = [list count];
    pagerControl.hidden = ([list count] < 2);
    
//    if(!pagerControl.hidden){
//        id firstItem = list[0];
//        id lastItem = [list lastObject];
//        
//        [list insertObject:lastItem atIndex:0];
//        
//        // Add the copy of the first item to the end
//        [list addObject:firstItem];
//    }
    
    [collectionlistV reloadData];
    [collectionlistV setContentOffset:CGPointZero animated:NO];
    
    [sectionloaderAIV stopAnimating];
    noOffersL.hidden = !([list count] == 0);
    collectionlistV.hidden = !noOffersL.hidden;
    
    noOffersL.text = noOffersL.hidden ? @"Currently, we are not running any offers" : @"";
}


#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [list count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return layoutsize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // set cell
    OfferCell *cell = (OfferCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    //Reposition elements
    {
        
        cell.mainV.frame = [UpdateFrame setSizeForView:cell.mainV usingSize:layoutsize];
        cell.imageCV.frame = [UpdateFrame setSizeForView:cell.imageCV usingSize:layoutsize];
        cell.mainImgV.frame = [UpdateFrame setSizeForView:cell.mainImgV usingSize:layoutsize];
        cell.placeholderImgV.frame = [UpdateFrame setSizeForView:cell.placeholderImgV usingSize:layoutsize];
    }
    NSDictionary *item = list[indexPath.row];
    [cell configureDataForCell:item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
//    NSDictionary *item = list[indexPath.row];
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}

#pragma mark - Scroll

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(banner_position != BANNER_POSITION_BOTTOM)
    {
        NSUInteger page = floor((scrollView.contentOffset.x - layoutsize.width / 2) / layoutsize.width) + 1;
        if (pageIndex != page)
        {
            pageIndex = page ;
            pagerControl.currentPage = pageIndex;
        }
    }
    
}


#pragma mark - API

-(void)response:(NSMutableDictionary *)response forAPI:(NSString *)apiName
{
    if(![ReadData isValidResponseData:response])
    {
        {
            NSString *message = [ReadData stringValueFromDictionary:response forKey:RESPONSEKEY_ERRORMESSAGE];
            [appD showAPIErrorWithMessage:message andTitle:ALERT_TITLE_ERROR];
            [self.view setUserInteractionEnabled:YES];
            [sectionloaderAIV stopAnimating];
        }
        return;
    }
    
    if ([apiName isEqualToString:API_GET_OFFERS]) {
        [self processResponse:response];
    }
}

@end
